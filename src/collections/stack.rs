pub trait Stack<T> {
	fn push(&mut self, x: T);
	fn pop(&mut self) -> Option<T>;
}

pub struct SimpleStack<T> {
	data: Vec<T>,
	capacity: usize,
	size: usize,
}
impl<T> SimpleStack<T>
where
	T: Default + Copy,
{
	fn new() -> SimpleStack<T> {
		SimpleStack {
			data: vec![T::default(); 10],
			capacity: 10,
			size: 0,
		}
	}
	fn grow(&mut self) {
		self.capacity *= 2;
		let mut new_data = vec![T::default(); self.capacity];
		for i in 0..self.size {
			new_data[i] = self.data[i];
		}
	}
}

impl<T> Stack<T> for SimpleStack<T>
where
	T: Default + Copy,
{
	fn push(&mut self, x: T) {}
	fn pop(&mut self) -> Option<T> {
		None
	}
}

// impl<T> Stack<T> for Vec<T> {
// 	fn push(&mut self, x: T) {
// 		self.push(x)
// 	}
// 	fn pop(&mut self) -> Option<T> {
// 		self.pop()
// 	}
// }

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_stack() {
		let mut stack = SimpleStack::new();
		stack.push(3);
		stack.push(7);
		stack.push(4);
		assert_eq!(stack.pop(), Some(4));
		assert_eq!(stack.pop(), Some(7));
		assert_eq!(stack.pop(), Some(3));
		assert_eq!(stack.pop(), None);
	}
	#[test]
	fn test_stack_as_trait() {
		let mut stack = SimpleStack::new();
		test_stack_trait(&mut stack);
	}
	fn test_stack_trait(stack: &mut dyn Stack<i32>) {
		stack.push(3);
		stack.push(7);
		stack.push(4);
		assert_eq!(stack.pop(), Some(4));
		assert_eq!(stack.pop(), Some(7));
		assert_eq!(stack.pop(), Some(3));
		assert_eq!(stack.pop(), None);
	}
	// #[test]
	// fn test_vec_stack() {
	// 	let mut stack: Vec<i32> = vec![];
	// 	test_stack_trait(&mut stack);
	// }
	// #[test]
	// fn test_vec_string() {
	// 	let mut stack: Box<dyn Stack<&str>> = Box::new(vec![]);
	// 	stack.push("3");
	// 	stack.push("7");
	// 	assert_eq!(stack.pop(), Some("7"));
	// 	assert_eq!(stack.pop(), Some("3"));
	// 	assert_eq!(stack.pop(), None);
	// }
}
