pub trait List<T>
where
	T: Eq,
{
	fn add(&mut self, x: T);
	fn contains(&self, x: T) -> bool;
	fn apply_mut(&mut self, x: fn(&mut T));
	fn get(&self, i: usize) -> Option<&T>;
}

pub struct LinkedList<T> {
	node: Option<Node<T>>,
}
struct Node<T> {
	data: T,
	next: Option<Box<Node<T>>>,
}
impl<T> LinkedList<T> {
	pub fn new() -> LinkedList<T> {
		LinkedList { node: None }
	}
}
impl<T> List<T> for LinkedList<T>
where
	T: Eq,
{
	fn add(&mut self, x: T) {
		let new_node = Node {
			data: x,
			next: None,
		};
		if self.node.is_none() {
			self.node = Some(new_node);
			return;
		}
		let mut cur = self.node.as_mut().unwrap();
		loop {
			if cur.next.is_none() {
				cur.next = Some(Box::new(new_node));
				return;
			}
			cur = cur.next.as_mut().unwrap();
		}
	}
	fn contains(&self, x: T) -> bool {
		if self.node.is_none() {
			return false;
		}
		let mut cur = self.node.as_ref().unwrap();
		loop {
			if cur.data == x {
				return true;
			}
			if cur.next.is_none() {
				return false;
			}
			cur = cur.next.as_ref().unwrap();
		}
	}
	fn get(&self, i: usize) -> Option<&T> {
		if self.node.is_none() {
			return None;
		}
		let mut cur = self.node.as_ref().unwrap();
		let mut loc_i = i;
		while loc_i > 0 {
			if cur.next.is_none() {
				return None;
			}
			cur = cur.next.as_ref().unwrap();
			loc_i -= 1;
		}
		Some(&cur.data)
	}
	fn apply_mut(&mut self, f: fn(&mut T)) {
		if self.node.is_none() {
			return;
		}
		let mut cur = self.node.as_mut().unwrap();
		loop {
			f(&mut cur.data);
			if cur.next.is_none() {
				return;
			}
			cur = cur.next.as_mut().unwrap();
		}
	}
}
pub enum EnumList<T> {
	Data(T, Box<EnumList<T>>),
	Null,
}
impl<T> EnumList<T> {
	pub fn new() -> EnumList<T> {
		EnumList::Null
	}
}
impl<T> List<T> for EnumList<T>
where
	T: Eq,
{
	fn add(&mut self, x: T) {
		match self {
			EnumList::Data(_, rest) => rest.add(x),
			EnumList::Null => *self = EnumList::Data(x, Box::new(EnumList::Null)),
		}
	}
	fn contains(&self, x: T) -> bool {
		match self {
			EnumList::Data(head, rest) => match *head == x {
				true => true,
				false => rest.contains(x),
			},
			EnumList::Null => false,
		}
	}
	fn get(&self, i: usize) -> Option<&T> {
		match (self, i) {
			(EnumList::Data(x, _), 0) => Some(x),
			(EnumList::Data(_, next), x) => next.get(x - 1),
			(EnumList::Null, _) => None,
		}
	}
	fn apply_mut(&mut self, f: fn(&mut T)) {
		if let EnumList::Data(data, rest) = self {
			f(data);
			rest.apply_mut(f)
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::add_five;

	#[test]
	fn test_linked_list() {
		let mut l = LinkedList::new();
		list_test(&mut l);
	}
	#[test]
	fn test_enum_list() {
		let mut l = EnumList::new();
		list_test(&mut l);
	}

	fn list_test(l: &mut dyn List<i32>) {
		l.add(3);
		l.add(7);
		l.add(4);
		l.apply_mut(add_five);
		assert_eq!(l.get(0), Some(&8));
		assert_eq!(l.get(1), Some(&12));
		assert_eq!(l.get(2), Some(&9));
		assert_eq!(l.get(3), None);
	}

	#[test]
	fn test_direct_enum_list() {
		let mut l = EnumList::new();
		l.add(3);
		l.add(7);
		l.add(4);
		l.apply_mut(add_five);
		assert_eq!(l.get(0), Some(&8));
		assert_eq!(l.get(1), Some(&12));
		assert_eq!(l.get(2), Some(&9));
		assert_eq!(l.get(3), None);
	}
}
