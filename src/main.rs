mod collections;

use collections::List;

fn add_five(x: &mut i32) {
    let temp = *x + 5;
    *x = temp;
}
fn print_string(x: &mut String) {
    println!("{}", x);
}
fn print_int(x: &mut i32) {
    println!("x={}", x);
}
fn print_item<T>(x: &mut T)
where
    T: std::fmt::Display,
{
    println!("{}", x);
}

fn new_enum() -> Box<dyn List<i32>> {
    Box::new(collections::EnumList::new())
}

fn main() {
    println!("enum");
    let mut l = new_enum();
    l.add(3);
    l.add(7);
    l.add(4);
    l.apply_mut(print_item);
    // l.apply_mut(add_five);
    l.apply_mut(print_item);

    let mut l: collections::EnumList<String> = collections::EnumList::new();
    l.add("123".to_string());
    l.add("hello".to_string());
    l.add("other".to_string());
    l.apply_mut(print_item);
}
